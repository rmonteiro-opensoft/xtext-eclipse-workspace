package pt.opensoft.generator;

import pt.opensoft.xpath.AdditiveExpr;
import pt.opensoft.xpath.AndExpr;
import pt.opensoft.xpath.ComparisonExpr;
import pt.opensoft.xpath.CountFunction;
import pt.opensoft.xpath.Domainmodel;
import pt.opensoft.xpath.Element;
import pt.opensoft.xpath.Function;
import pt.opensoft.xpath.NotExpr;
import pt.opensoft.xpath.NumericLiteral;
import pt.opensoft.xpath.OrExpr;
import pt.opensoft.xpath.PathExpr;
import pt.opensoft.xpath.StringLiteral;
import pt.opensoft.xpath.SumFunction;
import pt.opensoft.xpath.ThenElse;
import pt.opensoft.xpath.ValidationPath;
import pt.opensoft.xpath.ValueExpr;
import pt.opensoft.xpath.XPath;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Generator {


	private static final String SUFIX_SUM = "Sum";
	private static final String SUFIX_COUNT = "Count";


	private static String getPath(ValidationPath validationPath) {
		StringBuilder path = new StringBuilder(validationPath.getPath());
		List<String> pathList = validationPath.getPath_list();
		pathList.forEach(el -> path.append("/").append(el));
		return path.toString();

	}

	public static Map<String, List<Result>> compileDomainmodel(Domainmodel domainModel) {
		List<XPath> elementsList = domainModel.getElements();
		List<ValidationPath> validationPathList = domainModel.getValidationPath();


		List<Result> resultList = new LinkedList<>();
		for (int i = 0; i < elementsList.size(); i++) {
			String path = Generator.getPath(validationPathList.get(i));

			resultList.add(compileXPath(elementsList.get(i), path));
		}


		Map<String, List<Result>> map = new HashMap<>();
		resultList.forEach(result -> {
			List<Result> list = map.get(result.path);
			if (list == null) {
				list = new LinkedList<>();
			}
			result.validationName += list.size();
			list.add(result);
			map.put(result.path, list);
		});

		for (Map.Entry<String, List<Result>> stringListEntry : map.entrySet()) {
			List<Result> value = stringListEntry.getValue();
		}

		return map;
	}


	public static class Result {
		public List<String> elementsToStore;
		public List<String> elementsVarNameList;
		public List<String> elementsToSum;
		public List<String> elementsToSumVarNameList;
		public List<String> elementsToCount;
		public List<String> elementsToCountVarNameList;
		public String elementsJoinedString;
		public String elementsToSumJoinedString;
		public String elementsToCountJoinedString;
		public String validationName;
		public String path;
		public String condition;
		public boolean thenCondition;
		public boolean elseCondition;
		public String errorMsg;

	}

	//[1]
	private static Result compileXPath(XPath xpath, String path) {
		String validationName = "validate" + path.replace("/", "");
		OrExpr conditionExpr = xpath.getExpr().getExprSingle().getIfExpr().getConditionExpr();
		ThenElse thenElse = xpath.getExpr().getExprSingle().getIfExpr().getThenElse();

		String condition = compileCondition(conditionExpr);

		ElementsGetter.Elements elements = ElementsGetter.getElements(conditionExpr);
		elements.elements = elements.elements.stream().distinct().collect(Collectors.toList());
		elements.elementsToSum = elements.elementsToSum.stream().distinct().collect(Collectors.toList());
		elements.elementsToCount = elements.elementsToCount.stream().distinct().collect(Collectors.toList());

		String elementsJoinedString = elements.elements.stream()
				.map(el -> "\"" + el + "\"")
				.collect(Collectors.joining(","));
		String elementsToSumJoinedString = elements.elementsToSum.stream()
				.map(el -> "\"" + el + "\"")
				.collect(Collectors.joining(","));
		String elementsToCountJoinedString = elements.elementsToCount.stream()
				.map(el -> "\"" + el + "\"")
				.collect(Collectors.joining(","));

		Result result = new Result();
		result.elementsToStore = elements.elements;
		result.elementsToSum = elements.elementsToSum;
		result.elementsToCount = elements.elementsToCount;
		
		result.elementsVarNameList = elements.elements.stream().map(el -> decapitalize(el).replace("/","")).collect(Collectors.toList());
		result.elementsToSumVarNameList = elements.elementsToSum.stream().map(el -> decapitalize(el).replace("/","") + SUFIX_SUM).collect(Collectors.toList());
		result.elementsToCountVarNameList = elements.elementsToCount.stream().map(el -> decapitalize(el).replace("/","") + SUFIX_COUNT).collect(Collectors.toList());

		result.elementsJoinedString = elementsJoinedString;
		result.elementsToSumJoinedString = elementsToSumJoinedString;
		result.elementsToCountJoinedString = elementsToCountJoinedString;
		result.validationName = validationName;
		result.path = path;
		result.condition = condition;
		result.thenCondition = thenElse.getThenExprTrue() != null;
		result.elseCondition = thenElse.getElseExprTrue() != null;
		if (thenElse.getThenExprTrue() != null) {
			result.errorMsg = thenElse.getElseExprFalse().getMsg();
		}
		else {
			result.errorMsg = thenElse.getThenExprFalse().getMsg();
		}
		if (result.errorMsg == null || result.errorMsg.isEmpty()) {
			result.errorMsg = "False";
		}

		return result;

	}

	private static String mapperJoin(String joiner){
		if (joiner == null) {
			return "";
		}
		switch (joiner.trim()) {
			case "and":
				return " && ";
			case "or":
				return " || ";
			default:
				return null;
		}
	}

	private static String compileCondition(OrExpr conditionExpr) {
		if (conditionExpr == null) {
			return "";
		}
		OrExpr groupedCondition = conditionExpr.getGroupedCondition();
		OrExpr rightGroupedCondition = conditionExpr.getRightGroupedCondition();
		if (groupedCondition != null) {
			String operator = conditionExpr.getOperator();
			OrExpr orExpr = conditionExpr.getOrExpr();

			return "( " + compileCondition(groupedCondition) + " )" + mapperJoin(operator) + compileCondition(orExpr);
		}
		else if(rightGroupedCondition != null) {
			AndExpr andExpr = conditionExpr.getAndExpr();
			String operator = conditionExpr.getOperator();

			return  compile(andExpr) + mapperJoin(operator) + "( " + compileCondition(rightGroupedCondition) + " )";
		}
		else {
			AndExpr andExpr = conditionExpr.getAndExpr();

			StringBuilder condition = compile(andExpr);

			List<AndExpr> orExprList = conditionExpr.getOrExpr_list();
			if (orExprList != null && ! orExprList.isEmpty()) {

				orExprList.forEach(expr ->
						condition.append(" || ")
								.append(compile(expr))
				);

			}
			return condition.toString();

		}
	}


	private static StringBuilder compile(AndExpr andExpr) {
		ComparisonExpr comparisonExpr = andExpr.getComparisonExpr();

		StringBuilder condition = new StringBuilder(compile(comparisonExpr));
		List<ComparisonExpr> andConditionList = andExpr.getAndCondition_list();
		if (andConditionList != null && ! andConditionList.isEmpty()) {
			andConditionList.forEach(expr ->
					condition.append(" && ")
							.append(compile(expr))
			);
		}

		return condition;
	}

	private static StringBuilder compile(ComparisonExpr comparisonExpr) {
		Function function = comparisonExpr.getFunction();
		if (function != null) {
			return compileFunction(function);
		}

		NotExpr notExpr = comparisonExpr.getNotExpr();
		if (notExpr != null) {
			return compile(notExpr);
		}
		else {
			AdditiveExpr additiveExpr = comparisonExpr.getAdditiveExpr();
			StringBuilder firstElement = compileAdditiveExpr(additiveExpr, true);
			StringBuilder condition = new StringBuilder();
			AdditiveExpr additiveExprOptional = comparisonExpr.getAdditiveExprOptional();
			if (additiveExprOptional != null) {

				firstElement = compileAdditiveExpr(additiveExpr, false);

				StringBuilder secondElement = compileAdditiveExpr(additiveExprOptional, false);


				String comparator = comparisonExpr.getGeneralCompOptional();
				if (comparator != null) {
					comparator = convert(comparator);

					condition.append(convertToNumerical(firstElement.toString())).append(comparator).append(" ").append(secondElement.toString());
				}
				String valueCompOptional = comparisonExpr.getValueCompOptional();
				if (valueCompOptional != null) {
					if (valueCompOptional.equals("eq")) {	//pode ser string
						condition.append("Objects.equals(").append(decapitalize(firstElement.toString())).append(", ").append(secondElement.toString()).append(") ");
					}
					else if(valueCompOptional.equals("ne")){
						condition.append("! Objects.equals(").append(decapitalize(firstElement.toString())).append(", ").append(secondElement.toString()).append(") ");
					}
					else {
						comparator = convert(comparator);

						condition.append(firstElement.toString()).append(" ")
								.append(comparator).append(" ")
								.append(secondElement.toString());
					}
				}


				String nodeCompOptional = comparisonExpr.getNodeCompOptional();			//TODO[RACM]

				if (nodeCompOptional != null) {
					throw new RuntimeException("Not implemented: nodeCompOptional != null");
				}

			}
			else{
				condition = firstElement;
			}


			return condition;
		}

	}


	private static StringBuilder compileAdditiveExpr(AdditiveExpr additiveExpr, boolean addIsBlankValidation) {
		ValueExpr valueExpr = additiveExpr.getValueExpr();

		List<ValueExpr> valueExprList = additiveExpr.getValueExpr_list();
		if (valueExprList != null && ! valueExprList.isEmpty()) {
			StringBuilder stringBuilder = new StringBuilder(compilePathExpr(valueExpr.getPathExpr(), addIsBlankValidation));
			List<String> additiveList = additiveExpr.getAdditive_list();

					//TODO[RACM] como tratar dos tipos? inteiro, long?
			int i = 0;
			for (ValueExpr expr : valueExprList) {
				stringBuilder.append(" ").append(additiveList.get(i)).append(" ");
				stringBuilder.append(convertToNumerical(compilePathExpr(expr.getPathExpr(), addIsBlankValidation)));
				i++;
			}

			return stringBuilder;
		}
		else {
			return compileValueExpr(valueExpr, addIsBlankValidation);
		}

	}

	private static String convertToNumerical(String varName) {
		return "Long.parseLong(" + decapitalize(varName) + ")";
	}


	private static StringBuilder compileValueExpr(ValueExpr valueExpr, boolean addIsBlankValidation) {
		NumericLiteral numericLiteral = valueExpr.getNumericLiteral();
		if (numericLiteral != null) {
			return new StringBuilder(String.valueOf(numericLiteral.getNumber()));
		}

		StringLiteral stringliteral = valueExpr.getStringliteral();
		if (stringliteral != null) {
			return new StringBuilder("\"" +stringliteral.getName()+ "\"");
		}

		PathExpr pathExpr = valueExpr.getPathExpr();
		if (pathExpr != null) {
			if (! addIsBlankValidation) {
				return new StringBuilder(compilePathExpr(pathExpr, addIsBlankValidation));
			}
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.append(compilePathExpr(pathExpr, addIsBlankValidation));

			return stringBuilder;
		}

		throw new RuntimeException("compileValueExpr n�o � v�lido");
	}

	private static StringBuilder compileFunction(Function function) {
		CountFunction countFunction = function.getCountFunction();

		PathExpr element;
		PathExpr resultElement;
		String sufix;
		if (countFunction != null) {
			element = countFunction.getCountElement();
			resultElement = countFunction.getResultElement();
			sufix = SUFIX_COUNT;
		}
		else {
			SumFunction sumFunction = function.getSumFunction();
			element = sumFunction.getSumElement();
			resultElement = sumFunction.getResultElement();
			sufix = SUFIX_SUM;
		}

		String count = compilePathExpr(element, false);
		String result = compilePathExpr(resultElement, false);


		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("Objects.equals(").append(decapitalize(count)).append(sufix).append(", ").append(decapitalize(result)).append(") ");
		return stringBuilder;

	}

	private static String compile(ValueExpr valueExpr, boolean addIsBlankValidation) {
		PathExpr pathExpr = valueExpr.getPathExpr();
		return compilePathExpr(pathExpr, addIsBlankValidation);
	}

	private static StringBuilder compile(NotExpr notExpr) {
		PathExpr pathExpr = notExpr.getPathExpr();

		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("StringUtils.isBlank(");
		stringBuilder.append(decapitalize(compilePathExpr(pathExpr, false)));

		stringBuilder.append(")");
		return stringBuilder;
	}


	private static String compilePathExpr(PathExpr pathExpr, boolean addIsBlankValidation) {
		Element element = pathExpr.getElement();
		List<Element> pathElementList = pathExpr.getPathElement_list();
		if (pathElementList != null && ! pathElementList.isEmpty()) {
			StringBuilder path = new StringBuilder(element.getName());
			pathElementList.forEach(el -> path.append(el.getName()));
			if (addIsBlankValidation) {
				return "StringUtils.isNotBlank(" + decapitalize(path.toString()) + ")";
			}
			return path.toString();

		}
		else {
			if (addIsBlankValidation) {
				return "StringUtils.isNotBlank("+ decapitalize(element.getName()) + ")";
			}

			return decapitalize(element.getName());
//			else if(element.getStringliteral()!= null){
//				return "\"" +element.getStringliteral().getName()+ "\"";
//			}
//			else {	//if(element.getNumericLiteral() != null)
//				return String.valueOf(element.getNumericLiteral().getNumber());
//			}
		}

	}

	private static String decapitalize(String word) {
		return Character.toLowerCase(word.charAt(0)) + word.substring(1);
	}



	private static String convert(String comparator){
		switch (comparator) {
			case "eq":
				return "=";

			case "ne":
				return "!=";

			case "lt":
				return "<";

			case "le":
				return "<=";

			case "gt":
				return ">";

			case "ge":
				return ">=";

			default:
				return comparator;
		}

	}
}