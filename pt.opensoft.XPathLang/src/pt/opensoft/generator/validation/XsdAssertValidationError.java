package pt.opensoft.generator.validation;


public class XsdAssertValidationError {
	private String error;
	private String path;

	public XsdAssertValidationError(String error, String path) {
		this.error = error;
		this.path = path;
	}

	public String getError() {
		return error;
	}

	public String getPath() {
		return path;
	}

}