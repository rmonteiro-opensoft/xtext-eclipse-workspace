package pt.opensoft.generator.validation.test;

import java.util.List;

import pt.opensoft.generator.validation.XsdAssertValidationError;

public class ValidationsTest {


	public static void main(String[] args) {
		testPositive();
		System.out.println("--------");
		testNegative();
	}


	private static void testNegative(){

		XsdAssertsValidator xsdAssertsValidator = new XsdAssertsValidator();

		String currentPath = "SourceDocuments/SalesInvoices/Invoice";
		xsdAssertsValidator.startElement(currentPath);

		currentPath = "SourceDocuments/SalesInvoices/Invoice/TotalPrice";
		xsdAssertsValidator.startElement(currentPath);
		xsdAssertsValidator.endElement(currentPath, "100");

		currentPath = "SourceDocuments/SalesInvoices/Invoice/Line";
		xsdAssertsValidator.startElement(currentPath);

		currentPath = "SourceDocuments/SalesInvoices/Invoice/Line/LineNumber";
		xsdAssertsValidator.startElement(currentPath);
		xsdAssertsValidator.endElement(currentPath, "1");
		currentPath = "SourceDocuments/SalesInvoices/Invoice/Line/UnitPrice";
		xsdAssertsValidator.endElement(currentPath, "200");
		currentPath = "SourceDocuments/SalesInvoices/Invoice/Line";
		xsdAssertsValidator.endElement(currentPath, null);

		currentPath = "SourceDocuments/SalesInvoices/Invoice/Line";
		xsdAssertsValidator.startElement(currentPath);
		xsdAssertsValidator.endElement(currentPath, "1");
		currentPath = "SourceDocuments/SalesInvoices/Invoice/Line/UnitPrice";
		xsdAssertsValidator.endElement(currentPath, "200");


		currentPath = "SourceDocuments/SalesInvoices/Invoice";
		List<XsdAssertValidationError> errors = xsdAssertsValidator.endElement(currentPath, null);

		errors.forEach(err -> System.out.printf(err.getError()));
	}

	private static void testPositive(){

		XsdAssertsValidator xsdAssertsValidator = new XsdAssertsValidator();

		String currentPath = "SourceDocuments/SalesInvoices/Invoice";
		xsdAssertsValidator.startElement(currentPath);

		currentPath = "SourceDocuments/SalesInvoices/Invoice/TotalPrice";
		xsdAssertsValidator.startElement(currentPath);
		xsdAssertsValidator.endElement(currentPath, "400");

		currentPath = "SourceDocuments/SalesInvoices/Invoice/Line";
		xsdAssertsValidator.startElement(currentPath);

		currentPath = "SourceDocuments/SalesInvoices/Invoice/Line/LineNumber";
		xsdAssertsValidator.startElement(currentPath);
		xsdAssertsValidator.endElement(currentPath, "1");
		currentPath = "SourceDocuments/SalesInvoices/Invoice/Line/UnitPrice";
		xsdAssertsValidator.endElement(currentPath, "200");
		currentPath = "SourceDocuments/SalesInvoices/Invoice/Line";
		xsdAssertsValidator.endElement(currentPath, null);

		currentPath = "SourceDocuments/SalesInvoices/Invoice/Line";
		xsdAssertsValidator.startElement(currentPath);
		xsdAssertsValidator.endElement(currentPath, "1");
		currentPath = "SourceDocuments/SalesInvoices/Invoice/Line/UnitPrice";
		xsdAssertsValidator.endElement(currentPath, "200");


		currentPath = "SourceDocuments/SalesInvoices/Invoice";
		List<XsdAssertValidationError> errors = xsdAssertsValidator.endElement(currentPath, null);

		errors.forEach(err -> System.out.printf(err.getError()));
	}

}
