package pt.opensoft.generator.validation;

import java.math.BigInteger;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;

public class AssertValidation {


	private String path;
	private Map<String, String> xmlElementKeyAndValueMap;
	private Set<String> valuesToStore;
	private Set<String> valuesToSum;
	private Set<String> valuesToCount;
	private Predicate<Map<String, String>> predicate;
	private String errorMsg;


	public AssertValidation(String path, Set<String> valuesToStore, Set<String> valuesToSum, Set<String> valuesToCount, Predicate<Map<String, String>> predicate, String errorMsg) {
		this.path = path;
		this.valuesToStore = valuesToStore;
		this.valuesToSum = valuesToSum;
		this.valuesToCount = valuesToCount;
		this.predicate = predicate;
		this.xmlElementKeyAndValueMap = new HashMap<>(valuesToStore.size());
		this.errorMsg = errorMsg;
	}


	public String getPath() {
		return path;
	}

	public Map<String, String> getXmlElementKeyAndValueMap() {
		return xmlElementKeyAndValueMap;
	}

	public Predicate<Map<String, String>> getPredicate() {
		return predicate;
	}


	public void putValue(String path, String value){
		String relativePath = path.replace(this.path+ "/", "");
		if (valuesToStore.contains(relativePath)) {
			xmlElementKeyAndValueMap.put(relativePath, value);
		}
		else if(valuesToSum.contains(relativePath)) {
			String sumString = xmlElementKeyAndValueMap.get(relativePath);
			BigInteger sum = BigInteger.ZERO;
			if (sumString != null) {
				sum = new BigInteger(sumString);
			}
			sum = sum.add(new BigInteger(value));
			xmlElementKeyAndValueMap.put(relativePath, sum.toString());
		}
		else if(valuesToCount.contains(relativePath)) {
			String countString = xmlElementKeyAndValueMap.get(relativePath);
			long count = 0L;
			if (countString != null) {
				count = Long.parseLong(countString);
			}
			count++;
			xmlElementKeyAndValueMap.put(relativePath, String.valueOf(count));
		}

	}

	public String getErrorMsg() {
		return errorMsg;
	}
}
