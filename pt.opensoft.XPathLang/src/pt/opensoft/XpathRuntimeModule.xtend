/*
 * generated by Xtext 2.19.0
 */
package pt.opensoft


/**
 * Use this class to register components to be used at runtime / without the Equinox extension registry.
 */
class XpathRuntimeModule extends AbstractXpathRuntimeModule {
}
